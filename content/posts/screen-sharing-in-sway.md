---
title: "Screen Sharing in Sway"
date: 2021-02-25T20:47:23+01:00
draft: false
toc: true
images:
tags:
  - wayland
  - sway
---

Screen sharing is done with `xdg-desktop-portal-wlr` on Wayland.

It is available in the AUR [https://archlinux.org/packages/community/x86_64/xdg-desktop-portal-wlr/](https://archlinux.org/packages/community/x86_64/xdg-desktop-portal-wlr/).

The projects source can be found here [https://github.com/emersion/xdg-desktop-portal-wlr](https://github.com/emersion/xdg-desktop-portal-wlr).

Make sure `XDG_CURRENT_DESKTOP=sway` is set.

When correctly installed, `xdg-desktop-portal` should automatically invoke `xdg-desktop-portal-wlr` when needed.

This is not the case however if you use multiple outputs (screens). You need to pass the `-output=<name>` option to `xdg-desktop-portal-wlr`. The following script hands over the currently focused output. This output is the available as the only sharing option in applications like Microsoft Teams, Zoom and alike. The dialogues of some browsers look like there is nothing to share. If you click somewhere in the dialogue the screen to share will appear.

```bash
#! /usr/bin/env bash

# share-screen
# This script starts or restarts `xdg-desktop-portal` and
# `xdg-desktop-portal-wlr`. While doing so the currently focused output (read
# screen) is passed to `xdg-desktop-portal-wlr`. This makes the respective
# screen available for sharing in apps and websites that can do so.

export XDG_CURRENT_DESKTOP=sway

output=$(swaymsg -pt get_outputs | grep focused | awk '{print $2}')
/usr/lib/xdg-desktop-portal -r & /usr/lib/xdg-desktop-portal-wlr -r -o $output &
```

It is currently not possible to share individual windows since there is no way to determine window borders in a not `sway` specific manner in Wayland. See [https://github.com/emersion/xdg-desktop-portal-wlr/issues/12](https://github.com/emersion/xdg-desktop-portal-wlr/issues/12)

The browser might be configured or built properly for sharing to work properly. See [https://github.com/emersion/xdg-desktop-portal-wlr/wiki/Screencast-Compatibility](https://github.com/emersion/xdg-desktop-portal-wlr/wiki/Screencast-Compatibility)

Resources:

Screen capture test page: [https://mozilla.github.io/webrtc-landing/gum_test.html](https://mozilla.github.io/webrtc-landing/gum_test.html)

Good guide also for browser configuration: [https://bool3max.github.io/posts/sway_screenshare_guide/](https://bool3max.github.io/posts/sway_screenshare_guide/)

Early guide, the stuff now works without the `-git` packages [https://soyuka.me/make-screen-sharing-wayland-sway-work/](https://soyuka.me/make-screen-sharing-wayland-sway-work/)
