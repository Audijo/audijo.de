---
title: "Deploy Hugo site with Gitlab-CI using SFTP"
date: 2021-02-27T12:51:08+01:00
draft: false
tags:
  - Hugo
  - Gitlab
  - Docker
  - SFTP
---

My web server is only accepting connections via SFTP for file transfer. This
rendered many of the already described ways of continuous integration and
deployment useless for me. With wahooga's much appreciated [post](https://www.wahooga.com/hugo-gitlab)
on how to build and deploy a Hugo website using SCP I had a great place to start.

In order to automatically build and deploy your website created with
[Hugo](https://gohugo.io/) add the [`.gitlab-ci.yml`](https://gitlab.com/Audijo/audijo.de/-/blob/master/.gitlab-ci.yml)
of my site to your sites main source directory. This will enable Gitlab CI for
your website. Additionally you need to set four variables in the Gitlab CI/CD
settings.

  - `SSH_HOST`: The host name or IP of your server.
  - `SSH_HOST_KEY`: The hosts key like it is entered in your `known_hosts` file,
  once you have connected to your server.
  - `SSH_PRIVATE_KEY`: The private key you use to connect to the server. The
  corresponding public key must be added to the servers authorized keys. With
  [OpenSSH](https://www.ssh.com/ssh/openssh/), a key pair can be created using
  the [`ssh-keygen`](https://www.ssh.com/ssh/keygen/) tool. The public key can
  then be copied to a server using the
  [`ssh-copy-id`](https://www.ssh.com/ssh/copy-id) tool.
  - `SSH_USER`: The user name you use to connect to your web server.

![Gitlab CI variables](/img/deploy-hugo-with-gitlab-ci_1.png)

With the configuration and the variables in place, a commit pushed to the master
branch of your project triggers a build and an deploy of your website
automatically.
